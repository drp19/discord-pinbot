# Discord Pinbot

For servers where 50 pins limit is not enough. 

See more details in [bots.gg](https://discord.bots.gg/bots/733436947653066844)

![](https://cdn.discordapp.com/attachments/381123230490165262/733522141504471102/unknown.png) 

[Invite using this link](https://discord.com/oauth2/authorize?client_id=733436947653066844&scope=bot&permissions=314432)

## Cross compiling to linux (musl) from macOS
Assuming that you have [homebrew](https://brew.sh) installed, do:
```sh
brew install FiloSottile/musl-cross/musl-cross
rustup target add x86_64-unknown-linux-musl
export TARGET_CC=x86_64-linux-musl-gcc
cargo build --release --target=x86_64-unknown-linux-musl
```

## Building docker image (because I'm not going to upload this to docker hub lol)
(Be sure to run compilation targeting musl before you do this)

Assuming that you have Docker installed, do:
```sh
docker build -t="discord-pinbot" .
```

## Copying docker image to server
(Skip this step if building/running on same machine)

From your local machine:
```sh
docker save -o /path/to/saved/tar/file/filename.tar discord-pinbot
```

Then on server:
```sh
docker load -i /path/to/uploaded/tar/file
```

## Running docker image
```sh
docker run -d -v /path/to/your/config/folder:/bin/pinbot/config --name pinbot "discord-pinbot"
```

Make sure your config folder has `credentials.json5` file!!!

## Cross compiling to other architectures
Use [this](https://github.com/messense/rust-musl-cross)!
