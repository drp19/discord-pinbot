use std::path::Path;
use std::sync::Mutex;
use rusqlite::{ params, Connection };
use serenity::model::id::{ GuildId, ChannelId, MessageId };

pub enum DBError
{
	ConstraintViolation,
	// Error message is printed for variants below this line - no specific handling required from user
	GenericError,
	GenericRusqliteError(rusqlite::Error)
}
pub struct DatabaseHandler
{
	conn: Mutex<Connection>
}

impl DatabaseHandler
{
	/// Initializes DB connection
	/// Creates new file if file doesn't exist in path
	/// Creates new table if table `guild_id` does not exist
	/// Returns Ok(DatabaseHandler) if operation is successful
	/// Returns Err(rusqlite::Error) otherwise
	pub fn new<P: AsRef<Path>>(path: P) -> Result<DatabaseHandler, rusqlite::Error>
	{
		let conn =Connection::open(path)?;
		conn.execute(
			&format!(
				"{} {} {} {}",
				"CREATE TABLE IF NOT EXISTS guild_data",
				"(id INTEGER PRIMARY KEY AUTOINCREMENT,",
				"guild_id TEXT UNIQUE NOT NULL,",
				"channel_id TEXT NOT NULL)"
			),
			params![]
		)?;
		conn.execute(
			&format!(
				"{} {} {} {}",
				"CREATE TABLE IF NOT EXISTS pinned_msgs",
				"(id INTEGER PRIMARY KEY AUTOINCREMENT,",
				"guild_id TEXT NOT NULL,",
				"message_id TEXT NOT NULL)"
			),
			params![]
		)?;
		Ok(DatabaseHandler
		{
			conn: Mutex::new(conn)
		})
	}

	/// Binds guild_id with channel_id.
	/// Returns Ok(usize) if operation is successful
	/// Returns Err(DBError::ConstraintViolation) if guild_id already exists
	/// Otherwise, prints error to stderr and returns Err(DBError::GenericRusqliteError(rusqlite::Error))
	pub fn bind_guild_channel(&self, guild_id: &GuildId, channel_id: &ChannelId) -> Result<usize, DBError>
	{
		let guild_id = guild_id.as_u64().to_string();
		let channel_id = channel_id.as_u64().to_string();
		match self.conn.lock().unwrap().execute(
			"INSERT INTO guild_data (guild_id, channel_id) VALUES (?1, ?2)",
			params![guild_id, channel_id]
		)
		{
			Ok(u) => Ok(u),
			Err(err) => 
			{
				if let rusqlite::Error::SqliteFailure(e, _) = err // rusqlite doesn't provide specific sqlitefailure enums smh
				{
					if e.code == rusqlite::ErrorCode::ConstraintViolation
					{
						return Err(DBError::ConstraintViolation);
					}
				}
				eprintln!("THIS IS NOT A CRASH: A generic sqlite3 error has occured while binding guild_id with channel_id");
				eprintln!("Debug info: {:?}", err);
				Err(DBError::GenericRusqliteError(err))
			}
		}
	}

	/// Finds channel_id corresponding to guild_id
	/// Returns Ok(Some(ChannelId)) if found. Ok(None) if not found.
	/// Returns Err(DBError::GenericError) on string to u64 conversion failure.
	/// Returns Err(DBError::GenericRusqliteError(rusqlite::Error)) on other errors.
	pub fn get_channel_id(&self, guild_id: &GuildId) -> Result<Option<ChannelId>, DBError>
	{
		let guild_id = guild_id.as_u64().to_string();

		let conn = self.conn.lock().unwrap();
		let mut stmt = match conn.prepare("SELECT channel_id FROM guild_data WHERE guild_id=(?)")
		{
			Ok(stmt) => stmt,
			Err(e) => 
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while constructing SQL query for getting channel_id with guild_id");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		};

		let mut rows = match stmt.query(params![guild_id])
		{
			Ok(rows) => rows,
			Err(e) => 
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while querying channel_id with guild_id from database");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		};

		let row = match rows.next()
		{
			Ok(row) => row,
			Err(e) =>
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while getting database row containing channel_id");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		};

		let s = if let Some(row) = row
		{
			match row.get::<&str, String>("channel_id")
			{
				Ok(s) => s,
				Err(e) => 
				{
					eprintln!("THIS IS NOT A CRASH: An error has occured while getting channel_id information from the returned row");
					eprintln!("Debug info: {:?}", e);
					return Err(DBError::GenericRusqliteError(e));
				}
			}
		}
		else
		{
			return Ok(None)
		};

		if let Ok(u) = s.parse::<u64>()
		{
			Ok(Some(ChannelId(u)))
		}
		else
		{
			eprintln!("THIS IS NOT A CRASH: An error has occurd while converting channel_id to channel_id object");
			eprintln!("Debug info: {:?}", s);
			Err(DBError::GenericError)
		}
	}

	/// Deletes guilds corresponding to guild_id
	/// returns Ok(usize) on success
	/// returns Err(DBError::GenericRusqliteError(rusqlite::Error)) on failure
	pub fn delete_guild(&self, guild_id: &GuildId) -> Result<usize, DBError>
	{
		let guild_id = guild_id.as_u64().to_string();
		match self.conn.lock().unwrap().execute(
			"DELETE FROM guild_data WHERE guild_id=(?)",
			params![guild_id]
		)
		{
			Ok(u) => Ok(u),
			Err(e) => 
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while deleting guild {} from DB.", guild_id);
				eprintln!("Debug info: {:?}", e);
				Err(DBError::GenericRusqliteError(e))
			}
		}
	}

	pub fn create_pin(&self, guild_id: &GuildId, message_id: &MessageId) -> Result<usize, DBError>
	{
		let guild_id = guild_id.as_u64().to_string();
		let message_id = message_id.as_u64().to_string();
		match self.conn.lock().unwrap().execute(
			"INSERT INTO pinned_msgs (guild_id, message_id) VALUES (?1, ?2)",
			params![guild_id, message_id]
		)
		{
			Ok(u) => Ok(u),
			Err(err) => 
			{
				eprintln!("THIS IS NOT A CRASH: A generic sqlite3 error has occured while binding guild_id with channel_id");
				eprintln!("Debug info: {:?}", err);
				Err(DBError::GenericRusqliteError(err))
			}
		}
	}

	pub fn is_pinned(&self, guild_id: &GuildId, message_id: &MessageId) -> Result<bool, DBError>
	{
		let guild_id = guild_id.as_u64().to_string();
		let message_id = message_id.as_u64().to_string();

		let conn = self.conn.lock().unwrap();

		let mut stmt = match conn.prepare("SELECT * FROM pinned_msgs WHERE guild_id=(?) AND message_id=(?)")
		{
			Ok(stmt) => stmt,
			Err(e) => 
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while constructing SQL query for getting pin status");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		};

		let mut rows = match stmt.query(params![guild_id, message_id])
		{
			Ok(rows) => rows,
			Err(e) => 
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while querying pin status from database");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		};

		match rows.next()
		{
			Ok(row) => match row
			{
				Some(_) => Ok(true),
				_ => Ok(false)
			}
			Err(e) =>
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while getting database row containing pin status");
				eprintln!("Debug info: {:?}", e);
				return Err(DBError::GenericRusqliteError(e));
			}
		}
	}
}