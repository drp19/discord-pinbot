use std::fs;
use json5;
use serde_derive::Deserialize;
use serenity::prelude::*;

mod database_handler;
use database_handler::DatabaseHandler;

mod event_handlers;

#[derive(Deserialize, Debug)]
struct Credentials
{
	token: String,
	motd: String,
}

#[tokio::main]
async fn main()
{
	// read credentials
	let credentials: Credentials = match fs::read_to_string("config/credentials.json5")
	{
		Ok(s) =>
		{
			json5::from_str(&s)
				.expect("FATAL: Credentials file is corrupted. Please delete it and create it again (refer to example.credentials.json5).")
		},
		Err(e) => panic!(format!("FATAL: Unable to read the credentials file. Debug info: {:?}", e))
	};

	// initialize database
	let db = DatabaseHandler::new("config/data.sqlite3")
		.expect("FATAL: An error has occured while accessing the database");

	// validate token
	if let Err(e) = serenity::client::validate_token(&credentials.token)
	{
		panic!(format!("FATAL: Provided token is invalid! Debug: {:?}", e));
	}

	// read motd
	let motd = if credentials.motd.is_empty() { None } else { Some(credentials.motd) };

	// initialize client
	let mut client = Client::builder(&credentials.token)
		.event_handler(event_handlers::Handler::new(db, motd))
		.await
		.expect("FATAL: A problem has occured while creating the bot instance :/");
	
	// start a shard
	if let Err(e) = client.start().await
	{
		eprintln!("THIS IS NOT A CRASH: a generic error has occured");
		eprintln!("Debug info: {:?}", e);
	}
}