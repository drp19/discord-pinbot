# Using Alpine
FROM alpine:latest
# Set working directory
WORKDIR /bin/pinbot
# Copy built binary
COPY /target/aarch64-unknown-linux-musl/release/discord-pinbot /bin/pinbot/discord-pinbot
# Run pinbot
ENTRYPOINT ["/bin/pinbot/discord-pinbot"]
